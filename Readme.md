# Plot-Scanner

## Overview
Plot-Scanner is a tool designed to extract curves or graphs from images, such as those found in articles or websites. Given an input image and its general information (width, height, axis scales, corner coordinates), the goal is to export the XY data of the curves to a CSV file.

## Installation
To use Plot-Scanner, you'll need Python 3.x installed on your system. You can install the required dependencies using pip:

pip install -r requirements.txt

## Usage
1. Clone this repository to your local machine:

git clone https://gitlab.com/rija-baritiana-rep/plot-scanner.git

2. Navigate to the project directory:

cd plot-scanner

3. Run the main script, passing the path to the input image as an argument, or optionally, use the -c flag to specify that the image will be taken from the clipboard:

# From file
python plot-scanner.py path/to/input_image.jpg

# From clipboard
python plot-scanner.py -c

4. The XY data of the extracted curves will be saved to a CSV file named output.csv.

## Curve Extraction Steps
The curve extraction process involves the following steps:
1. Image Preprocessing: Convert the image to grayscale, apply noise reduction techniques, and threshold the image to create a binary representation.
2. Curve Detection: Use edge detection techniques to identify potential curves or lines in the image.
3. Curve Extraction: Extract XY data points from detected curves by finding endpoints or tracing contours.
4. Coordinate Mapping: Scale and translate pixel coordinates to match the actual data coordinates on the graph.
5. Export to CSV: Save the extracted XY data points to a CSV file.

## Contributing
Contributions to PlotScanner are welcome! If you have any ideas for improvements or find any issues, please open an issue or submit a pull request.

## License
This project is licensed under the MIT License - see the LICENSE file for details.

## Acknowledgements
- [OpenCV](https://opencv.org/) - Used for image processing tasks.
- [NumPy](https://numpy.org/) - Used for numerical computations.
- [PIL (Python Imaging Library)](https://python-pillow.org/) - Used for image manipulation and processing.
- [Matplotlib](https://matplotlib.org/) - Used for data visualization.