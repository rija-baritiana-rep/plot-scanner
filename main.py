import sys
import matplotlib.pyplot as plt

sys.path.insert(1, './utils')

import graph_detect as gd
import postprocess_img as ppi


# Example usage:
f_img = "img/fig_2-197.jpg"
offset = 2 #depends on the width of the bounding box in the original image
size_min = 50000 #minimum surface of the box (pixel x pixel)
cropped_img = gd.graph_detect(f_img,offset,size_min)

# Display the cropped image
plt.imshow(cropped_img)
plt.title("Cropped image")
plt.axis('off')
plt.show()

bot_left = (0.5,0.2)
top_right = (2.,5.)
axis ='log-log'

line = {'x':[1.45]}

ppi.draw_line_img(cropped_img,bot_left,top_right,line,axis,line_width=1.5)