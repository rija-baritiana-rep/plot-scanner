import matplotlib.pyplot as plt

def draw_line_img(img,bot_left,top_right,line,axis='linear-linear',line_width=5):
    """draw horizontal/vertical line on an image
    inputs : 
    -img : image as an array
    -bot_left : coordinate of the bottom-left corner
    -top_right : coordinate of the top-right corner
    -line : dictionnary of the line to draw (x : vertical line abscissa, y : horizontal line ordinate)
    -axis : scale of the axis (linear-linear scale (default), linear-log, log-linear, log-log)"""

    ax = plt.gca()

    if 'x' in line.keys():
        #Draw vertical line
        for xv in line['x']:
            ax.axvline(xv, lw = line_width, label=f"x = {xv}")
    if 'y' in line.keys():
        #Draw horizontal line
        for yh in line['y']:
            ax.axhline(yh, lw = line_width, label=f"y = {yh}")


    #Delimitation of the plot
    x0,y0 = bot_left
    x1,y1 = top_right

    if axis in ['linear-log','log-linear','log-log']:
        x_scale = axis.split('-')[0]
        y_scale = axis.split('-')[1]
    else:
        x_scale = 'linear'
        y_scale = 'linear'

    ax.set_xlabel('X')
    ax.set_xlim([x0,x1])
    ax.set_xscale(x_scale)
    ax.set_ylabel('Y')
    ax.set_ylim([y0,y1])
    ax.set_yscale(y_scale)
    ax.legend(loc='best')

    #Resolution of the image distorsion for log scale : https://stackoverflow.com/questions/60747109/how-to-properly-combine-a-log-scale-plot-with-a-background-image
    ax.set_zorder(2)
    ax.set_facecolor('none')

    ax_tw_x = ax.twinx()
    ax_tw_x.axis('off')
    ax2 = ax_tw_x.twiny()

    ax2.imshow(img[:,:,::-1],extent=[x0, x1, y0, y1], aspect='auto') #cv2.imshow : BGR , plt.imshow : RGB
    ax2.axis('off')

    plt.show()