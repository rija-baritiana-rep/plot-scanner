import cv2

def graph_detect(f_img,offset=0, size_min = 500):
    """Detect edge, draw a bounding box, return the largest bounding box
    inputs : 
    -f_img : image path
    -offset : offset added to the select image. If positive, the image gets smaller"""
    
    # Read the image
    img = cv2.imread(f_img)
    
    # Convert the image to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    # Apply edge detection
    edges = cv2.Canny(gray, 50, 150, apertureSize=3)
    
    # Find contours
    contours, _ = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    
    # Filter out small contours (assuming box size)
    large_contours = [cnt for cnt in contours if cv2.contourArea(cnt) > size_min]
    
    # Get the bounding box of the first large contour
    x, y, w, h = cv2.boundingRect(large_contours[0])
    
    # Crop the image based on the bounding box
    cropped_image = img[y+offset:y+h-offset, x+offset:x+w-offset]
    
    return cropped_image